package main

import (
	"bytes"
	"fmt"
	"os"
	"text/template"
	"time"

	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgb/xproto"
)

func main() {
	// Handle args
	if len(os.Args) != 2 {
		panic("Must supply one argument")
	}

	state := newState()
	funcMap := template.FuncMap{
		"battSummary": state.fnBattSummary,
		"netFileRx":   state.fnNetFileRx,
		"netFileTx":   state.fnNetFileTx,
		"timestamp":   state.fnTimestamp,
	}

	format, err := template.New("").Funcs(funcMap).Parse(os.Args[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	X, err := xgb.NewConn()
	if err != nil {
		panic(err)
	}
	root := xproto.Setup(X).DefaultScreen(X).Root

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	for t := range ticker.C {
		if err := state.update(t); err != nil {
			fmt.Fprintf(os.Stderr, "error updating status line: %v\n", err)
			continue
		}

		var buff bytes.Buffer
		if err := format.Execute(&buff, struct{}{}); err != nil {
			fmt.Fprintf(os.Stderr, "error updating status line: %v\n", err)
			continue
		}

		b := buff.Bytes()
		xproto.ChangeProperty(
			X,
			xproto.PropModeReplace,
			root,
			xproto.AtomWmName,
			xproto.AtomString,
			byte(8),
			uint32(len(b)),
			b,
		)
	}
}
