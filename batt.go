package main

import (
	"fmt"
	"os"

	"github.com/distatus/battery"
)

func (s *state) fnBattSummary(n int) string {
	batts, err := battery.GetAll()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return "XXXX"
	}
	if len(batts) < n+1 {
		fmt.Fprintf(os.Stderr, "No battery #%d\n", n)
		return "XXXX"
	}

	batt := batts[n]

	var status string = " "
	switch batt.State.String() {
	case "Full":
		status = " "
	case "Discharging":
		status = "-"
	case "Charging":
		status = "+"
	default:
		fmt.Fprintf(os.Stderr, "Unknown battery state: %s\n", batt.State)
	}

	return fmt.Sprintf("%03d%s", int(batt.Current/batt.Full*100), status)
}
