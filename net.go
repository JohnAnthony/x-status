package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type net struct {
	tx uint64
	rx uint64
}

func newNet(iface string) (*net, error) {
	read := func(path string) (uint64, error) {
		content, err := ioutil.ReadFile(path)
		if err != nil {
			return 0, err
		}

		ret, err := strconv.ParseUint(
			strings.TrimSpace(string(content)),
			10,
			64,
		)
		if err != nil {
			return 0, err
		}

		return ret, nil
	}

	tx, err := read(fmt.Sprintf("/sys/class/net/%s/statistics/tx_bytes", iface))
	if err != nil {
		return nil, err
	}
	rx, err := read(fmt.Sprintf("/sys/class/net/%s/statistics/rx_bytes", iface))
	if err != nil {
		return nil, err
	}

	return &net{tx: tx, rx: rx}, nil
}
