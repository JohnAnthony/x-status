package main

import (
	"fmt"
	"os"
	"time"

	"code.cloudfoundry.org/bytefmt"
)

type state struct {
	nowTime  time.Time
	prevTime time.Time
	nowNet   map[string]*net
	prevNet  map[string]*net
}

func newState() *state {
	return &state{
		prevNet: make(map[string]*net),
		nowNet:  make(map[string]*net),
	}
}

func (s *state) fnTimestamp() string {
	return s.nowTime.Format("2006-01-02 15:04:05 Mon")
}

func (s *state) fnNetFileTx(iface string) string {
	if err := s.ensureNet(iface); err != nil {
		fmt.Fprintf(os.Stderr, "err getting netFileTx %s: %v\n", iface, err)
		return "EEEEEE"
	}
	nowN := s.nowNet[iface]

	prevN, found := s.prevNet[iface]
	if !found {
		return "XXXXXX"
	}

	delta := nowN.tx - prevN.tx
	if delta < 4098 {
		return "------"
	}

	return fmt.Sprintf("%06s", bytefmt.ByteSize(delta))
}

func (s *state) fnNetFileRx(iface string) string {
	if err := s.ensureNet(iface); err != nil {
		fmt.Fprintf(os.Stderr, "err getting netFileRx %s: %v\n", iface, err)
		return "EEEEEE"
	}
	nowN := s.nowNet[iface]

	prevN, found := s.prevNet[iface]
	if !found {
		return "XXXXXX"
	}

	delta := nowN.rx - prevN.rx
	if delta < 4098 {
		return "------"
	}

	return fmt.Sprintf("%06s", bytefmt.ByteSize(delta))
}

func (s *state) ensureNet(iface string) error {
	if _, found := s.nowNet[iface]; found {
		return nil
	}

	newN, err := newNet(iface)
	if err != nil {
		return err
	}

	s.nowNet[iface] = newN
	return nil
}

func (s *state) update(t time.Time) error {
	newS := state{}
	newS.prevTime = s.nowTime
	newS.nowTime = t
	newS.prevNet = s.nowNet
	newS.nowNet = make(map[string]*net, len(newS.prevNet))

	*s = newS
	return nil
}
